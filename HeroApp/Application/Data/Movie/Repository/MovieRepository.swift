import RxSwift

struct MovieRepository {
  
  private let apiDataSource: MovieDataSource
  
  init(apiDataSource: MovieDataSource) {
    self.apiDataSource = apiDataSource
  }
  
  func searchMovie(with request: MovieSearchRequest) -> Single<PaginatedList<Movie>> {
    return apiDataSource.searchMovie(with: request)
  }
}
