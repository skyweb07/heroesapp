import Foundation

private enum PosterSize: String {
  case w92
  case w185
  case ​w500
}

private enum Image {
  static let baseUrl = "https://image.tmdb.org/t/p/"
}

struct MovieDomainMapper: Mappable {
  
  private let dateFormatter: DateFormatter
  
  init(dateFormatter: DateFormatter) {
    self.dateFormatter = dateFormatter
  }
  
  func map(_ from: MovieApiResponse) throws -> Movie {
    return Movie(
      name: from.name,
      overview: from.overview,
      posterUrl: posterUrl(from.posterPath),
      releaseDate: dateFormatter.date(from: from.releaseDate)
    )
  }
  
  private func posterUrl(_ path: String?) -> URL? {
    guard let path = path else { return nil}
    return URL(string: "\(Image.baseUrl)\(PosterSize.w185.rawValue)\(path)")!
  }
}
