import Foundation

struct PaginatedResponseDomainMapper {
  
  private let movieDomainMapper: MovieDomainMapper
  
  init(movieDomainMapper: MovieDomainMapper) {
    self.movieDomainMapper = movieDomainMapper
  }
  
  func map(_ from: PaginatedListApiResponse<MovieApiResponse>) throws -> PaginatedList<Movie> {
    let hasNextPage = from.currentPage < from.totalPages
    return PaginatedList(
      currentPage: from.currentPage,
      hasNextPage: hasNextPage,
      totalPages: from.totalPages,
      items: try movieDomainMapper.map(elements: from.items)
    )
  }
}
