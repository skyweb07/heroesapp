import RxSwift
import Moya

struct MovieApiDataSource: MovieDataSource {
  
  private let provider: MoyaProvider<MovieSearchService>
  private let paginatedResponseDomainMapper: PaginatedResponseDomainMapper
  
  init(provider: MoyaProvider<MovieSearchService>,
       paginatedResponseDomainMapper: PaginatedResponseDomainMapper)
  {
    self.provider = provider
    self.paginatedResponseDomainMapper = paginatedResponseDomainMapper
  }
  
  func searchMovie(with request: MovieSearchRequest) -> Single<PaginatedList<Movie>> {
    return provider.rx
      .request(.search(request.query, request.page))
      .filterSuccessfulStatusCodes()
      .map(PaginatedListApiResponse<MovieApiResponse>.self)
      .map(paginatedResponseDomainMapper.map)
  }
}
