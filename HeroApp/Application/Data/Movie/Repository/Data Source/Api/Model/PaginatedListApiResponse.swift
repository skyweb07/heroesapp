import Foundation

struct PaginatedListApiResponse<T: Codable>: Codable {
  let currentPage: Int
  let totalPages: Int
  let items: [T]
  
  enum CodingKeys: String, CodingKey {
    case currentPage = "page"
    case totalPages = "total_pages"
    case items = "results"
  }
}
