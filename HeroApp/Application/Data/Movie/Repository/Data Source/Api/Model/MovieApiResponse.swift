import Foundation

struct MovieApiResponse: Codable {
  let name: String
  let overview: String
  let posterPath: String?
  let releaseDate: String
  
  enum CodingKeys: String, CodingKey {
    case name = "title"
    case overview = "overview"
    case posterPath = "poster_path"
    case releaseDate = "release_date"
  }
}
