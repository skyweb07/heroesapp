import RxSwift

protocol MovieDataSource {
  func searchMovie(with request: MovieSearchRequest) -> Single<PaginatedList<Movie>>
}
