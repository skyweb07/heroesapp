import RxSwift

struct SuggestionRepository {
  
  private let diskDataSource: SuggestionDataSource
  
  init(diskDataSource: SuggestionDataSource) {
    self.diskDataSource = diskDataSource
  }
  
  func save(_ query: String) -> Completable {
    return diskDataSource.save(query)
  }
  
  func getAll() -> Single<[Suggestion]> {
    return diskDataSource.getAll()
  }
}
