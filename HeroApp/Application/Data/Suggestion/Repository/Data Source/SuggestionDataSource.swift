import RxSwift

protocol SuggestionDataSource {
  func save(_ query: String) -> Completable
  func getAll() -> Single<[Suggestion]>
}
