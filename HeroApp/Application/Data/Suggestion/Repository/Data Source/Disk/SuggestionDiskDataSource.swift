import RxSwift
import RealmSwift

struct SuggestionDiskDataSource: SuggestionDataSource {
  
  private let realm: Realm
  
  init(realm: Realm) {
    self.realm = realm
  }
  
  func save(_ query: String) -> Completable {
    return Completable.create(subscribe: { onComplete -> Disposable in
      
      let suggestion = SuggestionRealmModel()
      suggestion.value = query
      
      do {
        try self.realm.write {
          self.realm.add(suggestion, update: true)
        }
      } catch {
        onComplete(.error(error))
      }
      
      self.removeFirstSuggestionIfNeeded()
      
      onComplete(.completed)
      return Disposables.create()
    })
  }
  
  private func removeFirstSuggestionIfNeeded() {
    let maximumSuggestionsOnDisk = 10
    if suggestions.count >= maximumSuggestionsOnDisk {
      try? self.realm.write {
        realm.delete(suggestions.first!)
      }
    }
  }
  
  func getAll() -> Single<[Suggestion]> {
    let suggestions: [Suggestion] = self.suggestions.map { element in
      return Suggestion(value: element.value)
    }

    return Single.create(subscribe: { event -> Disposable in
      event(.success(suggestions))
      return Disposables.create()
    })
  }
  
  private var suggestions: Results<SuggestionRealmModel> {
    return realm.objects(SuggestionRealmModel.self)
  }
}
