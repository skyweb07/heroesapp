import RealmSwift

final class SuggestionRealmModel: Object {
  @objc dynamic var value = ""
  
  override static func primaryKey() -> String? {
    return "value"
  }
}
