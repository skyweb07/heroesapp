import Moya

enum MovieSearchService: TargetType {
  case search(String, Page)
}

extension MovieSearchService {
  var baseURL: URL {
    return Service.baseUrl
  }
  
  var path: String {
    return endpoint(for: self).path
  }
  
  var method: Moya.Method {
    return endpoint(for: self).method
  }
  
  var task: Task {
    return endpoint(for: self).task
  }
}

private func endpoint(for service: MovieSearchService) -> Endpoint {
  switch service {
  case .search(let query, let page):
    return SearchMovieEndpoint(query: query, page: page)
  }
}
