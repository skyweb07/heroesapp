import Moya
import Alamofire

// This doesn't belong here but we only have to
// call one endpoint so it make sense to leave it here
// The right way to do it it's to make an interceptor (plugin on moya) and
// add the api key to all the requests so it's only on 1 place
private enum Api {
  static let key = "2696829a81b1b5827d515ff121700838"
}

struct SearchMovieEndpoint: Endpoint {
  
  private let query: String
  private let page: Page
  
  init(query: String,
       page: Page)
  {
    self.query = query
    self.page = page
  }
  
  var path: String {
    return "/3/search/movie"
  }
  
  var method: Moya.Method {
    return .get
  }
  
  var task: Task {
    return .requestParameters(parameters: [
      "api_key": Api.key,
      "query": query,
      "page": page.number
    ], encoding: URLEncoding.default)
  }
}
