import Foundation
import RealmSwift

// MARK: - Use Case

extension Assembly {
  var saveSuggestion: SaveSuggestionUseCase {
    return SaveSuggestion(
      suggestionRepository: suggestionRepository
    )
  }
  
  var getSuggestions: GetSuggestionsUseCase {
    return GetSuggestions(
      suggestionRepository: suggestionRepository
    )
  }
}

// MARK: - Repository

extension Assembly {
  private var suggestionRepository: SuggestionRepository {
    return SuggestionRepository(
      diskDataSource: suggestionDiskSource
    )
  }
  
  var suggestionDiskSource: SuggestionDataSource {
    return SuggestionDiskDataSource(realm: realm)
  }
}

// MARK: - Realm

extension Assembly {
  private var realm: Realm {
    return try! Realm()
  }
}
