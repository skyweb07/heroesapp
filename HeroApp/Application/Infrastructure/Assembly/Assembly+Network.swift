import Moya

extension Assembly {
  func moya<T: TargetType>() -> MoyaProvider<T> {
    return MoyaProvider()
  }
}
