import Foundation

// MARK: - Use Case

extension Assembly {
  var searchMovie: SearchMovieUseCase {
    return SearchMovie(
      movieRepository: movieRepository
    )
  }
}

// MARK: - Repository

extension Assembly {
  private var movieRepository: MovieRepository {
    return MovieRepository(apiDataSource: movieApiDataSource)
  }
  
  var movieApiDataSource: MovieDataSource {
    return MovieApiDataSource(
      provider: moya(),
      paginatedResponseDomainMapper: paginatedListMapper
    )
  }
}

// MARK: - Mapper

extension Assembly {
  private var movieMapper: MovieDomainMapper {
    return MovieDomainMapper(
      dateFormatter: dateFormatter
    )
  }
  
  private var paginatedListMapper: PaginatedResponseDomainMapper {
    return PaginatedResponseDomainMapper(
      movieDomainMapper: movieMapper
    )
  }
}
