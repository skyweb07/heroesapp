import Moya

protocol Endpoint {
  var path: String { get }
  var method: Moya.Method { get }
  var task: Task { get }
  var headers: [String: String]? { get }
}

// MARK: - Default values

extension Endpoint {
  var headers: [String: String]? {
    return nil
  }
}
