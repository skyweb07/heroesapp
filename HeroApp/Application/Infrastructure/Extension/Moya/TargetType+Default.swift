import Moya

// Add default values for unused parameters on most moya services
extension TargetType {
  var sampleData: Data {
    return Data()
  }
  
  var headers: [String : String]? {
    return nil
  }
}
