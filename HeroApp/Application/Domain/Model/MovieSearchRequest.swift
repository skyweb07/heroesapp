import Foundation

struct MovieSearchRequest {
  let query: String
  let page: Page
}
