import Foundation

struct Page: Equatable {
  let number: Int
}
 
extension Page {
  static var first = Page(number: 1)
}

extension Page {
  var next: Page {
    return Page(number: number + 1)
  }
}
