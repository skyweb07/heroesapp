import Foundation

struct PaginatedList<T> {
  /// Get the current page for the request
  let currentPage: Int
  
  /// Check if there are more pages to keep paginating
  let hasNextPage: Bool
  
  /// Get the total number of available pages
  let totalPages: Int
  
  /// Get the items being paginated
  let items: [T]
}
