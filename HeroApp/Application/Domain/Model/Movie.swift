import Foundation

struct Movie {
  let name: String
  let overview: String
  let posterUrl: URL?
  let releaseDate: Date?
}
