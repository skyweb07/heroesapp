import RxSwift

protocol GetSuggestionsUseCase {
  func execute() -> Single<[Suggestion]>
}

struct GetSuggestions: GetSuggestionsUseCase {
  
  private let suggestionRepository: SuggestionRepository
  
  init(suggestionRepository: SuggestionRepository) {
    self.suggestionRepository = suggestionRepository
  }
  
  func execute() -> Single<[Suggestion]> {
    return suggestionRepository.getAll()
  }
}
