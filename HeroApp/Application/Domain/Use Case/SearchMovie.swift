import RxSwift

protocol SearchMovieUseCase {
  func searchMovie(with request: MovieSearchRequest) -> Single<PaginatedList<Movie>>
}

struct SearchMovie: SearchMovieUseCase {
  
  private let movieRepository: MovieRepository
  
  init(movieRepository: MovieRepository) {
    self.movieRepository = movieRepository
  }
  
  func searchMovie(with request: MovieSearchRequest) -> Single<PaginatedList<Movie>> {
    return movieRepository.searchMovie(with: request)
  }
}
