import RxSwift

protocol SaveSuggestionUseCase {
  func execute(with query: String) -> Completable
}

struct SaveSuggestion: SaveSuggestionUseCase {
  
  private let suggestionRepository: SuggestionRepository
  
  init(suggestionRepository: SuggestionRepository) {
    self.suggestionRepository = suggestionRepository
  }
  
  func execute(with query: String) -> Completable {
   return suggestionRepository.save(query)
  }
}
