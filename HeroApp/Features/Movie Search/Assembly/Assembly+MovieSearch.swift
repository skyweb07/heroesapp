import UIKit
import IGListKit

// MARK: - MovieSearchProvider

extension Assembly: MovieSearchProvider {
  func movieSearch() -> MovieSearchViewController {
    let viewController = MovieSearchViewController(
      listAdapter: listAdapter,
      listDataSource: listDataSource
    )
    viewController.presenter = presenter(for: viewController)
    viewController.searchController = searchController(viewController)
    return viewController
  }
  
  // MARK: - IGListKit
  
  private var listAdapter: ListAdapter {
    return ListAdapter(updater: ListAdapterUpdater(), viewController: nil)
  }
  
  private var listDataSource: MovieListDataSource {
    return MovieListDataSource()
  }
  
  // MARK: - Search controller
  
  private func searchController(_ from: MovieSearchViewController) -> UISearchController {
    let searchResultsController = suggestions()
    searchResultsController.delegate = from
    
    let searchController = UISearchController(
      searchResultsController: searchResultsController
    )
    searchController.searchBar.delegate = from
    return searchController
  }
  
  // MARK: - Presenter
  
  private func presenter(for viewController: MovieSearchViewController) -> MovieSearchPresenter {
    let presenter = MovieSearchPresenter(
      searchMovie: searchMovie,
      saveSuggestion: saveSuggestion,
      movieViewMapper: movieViewMapper,
      pageController: pageController
    )
    presenter.view = viewController
    viewController.presenter = presenter
    return presenter
  }
  
  private var pageController: PageController {
    return PageController()
  }
  
  // MARK: - View Mapper
  
  private var movieViewMapper: MovieViewMapper {
    return MovieViewMapper(
      dateFormatter: dateFormatter
    )
  }
}
