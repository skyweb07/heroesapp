import IGListKit

final class MovieViewModel {
  let name: String
  let overview: String
  let posterUrl: URL?
  let releaseDate: String
  
  init(name: String,
       overview: String,
       posterUrl: URL?,
       releaseDate: String)
  {
    self.name = name
    self.overview = overview
    self.posterUrl = posterUrl
    self.releaseDate = releaseDate
  }
}

// MARK: - ListDiffable

extension MovieViewModel: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    let identifier = "\(name)\(overview)\(releaseDate)"
    return identifier as NSObjectProtocol
  }
  
  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? MovieViewModel else { return false }
    return object.name == name &&
      object.overview == overview &&
      object.posterUrl == posterUrl &&
      object.releaseDate == releaseDate
  }
}
