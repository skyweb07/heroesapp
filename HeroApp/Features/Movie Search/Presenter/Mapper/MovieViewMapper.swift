import Foundation

struct MovieViewMapper: Mappable {
  
  private let dateFormatter: DateFormatter
  
  init(dateFormatter: DateFormatter) {
    self.dateFormatter = dateFormatter
  }
  
  func map(_ from: Movie) throws -> MovieViewModel {
    return MovieViewModel(
      name: from.name,
      overview: from.overview,
      posterUrl: from.posterUrl,
      releaseDate: releaseDate(from.releaseDate)
    )
  }
  
  private func releaseDate(_ from: Date?) -> String {
    guard let from = from else {
      return NSLocalizedString("movie_search.movie.unknwon_release_date", comment: "")
    }
    return dateFormatter.string(from: from)
  }
}
