import Foundation

final class PageController {
  
  private var pages = [PaginatedList<Movie>]()
  private var currentPage: Page?
  
  func restore() {
    pages.removeAll()
    currentPage = nil
  }
  
  @discardableResult
  func append(_ paginatedList: PaginatedList<Movie>, page: Page) -> PaginatedList<Movie> {
    pages.append(paginatedList)
    currentPage = page
    
    let mergedItems = pages.flatMap { $0.items }
    return PaginatedList(
      currentPage: paginatedList.currentPage,
      hasNextPage: paginatedList.hasNextPage,
      totalPages: paginatedList.totalPages,
      items: mergedItems
    )
  }
  
  func shouldLoadNextPage() -> Bool {
    guard let currentPage = pages.last else { return false }
    return currentPage.hasNextPage
  }
  
  var nextPage: Page {
    guard let currentPage = currentPage else { return .first }
    return currentPage.next
  }
}
