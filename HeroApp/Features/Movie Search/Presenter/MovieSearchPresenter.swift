import RxSwift

final class MovieSearchPresenter {
  
  weak var view: MovieSearchView?
  
  private let searchMovie: SearchMovieUseCase
  private let saveSuggestion: SaveSuggestionUseCase
  private let movieViewMapper: MovieViewMapper
  private let pageController: PageController
  private var currentQuery: String?
  private let bag = DisposeBag()
  
  init(searchMovie: SearchMovieUseCase,
       saveSuggestion: SaveSuggestionUseCase,
       movieViewMapper: MovieViewMapper,
       pageController: PageController)
  {
    self.searchMovie = searchMovie
    self.saveSuggestion = saveSuggestion
    self.movieViewMapper = movieViewMapper
    self.pageController = pageController
  }
  
  func onSearch(query: String) {
    currentQuery = query
    pageController.restore()
    restoreSearch()
    search()
  }
  
  func onLoadNextPage() {
    guard pageController.shouldLoadNextPage() else {
      view?.setNoMoreResults()
      return 
    }
    search()
  }
  
  // MARK: - Fetch page
  
  private func search() {
    guard let query = currentQuery else { return }
    
    let page = pageController.nextPage
    let searchRequest = MovieSearchRequest(query: query, page: page)

    searchMovie.searchMovie(with: searchRequest)
      .subscribe(onSuccess: { moviePage in
        self.handle(moviePage, page: page)
      }) { error in
        self.view?.set(error.localizedDescription)
        self.restoreSearch()
    }.disposed(by: bag)
  }
 
  private func handle(_ moviePage: PaginatedList<Movie>, page: Page) {
    let thereAreNoResults = moviePage.items.isEmpty && page == .first
    if thereAreNoResults {
      view?.setNoResults()
      return
    }

    let mergedMovies = pageController.append(moviePage, page: page)
    guard let movieResults = try? movieViewMapper.map(elements: mergedMovies.items) else { return }
    view?.set(movieResults)
    
    guard let query = currentQuery, page == .first else { return }
    saveSuggestion.execute(with: query)
      .subscribe()
      .disposed(by: bag)
  }
  
  private func restoreSearch() {
    view?.set([])
  }
}
