import Foundation

extension MovieSearchViewController {
  func configureSearchController() {
    navigationController?.navigationBar.prefersLargeTitles = true
    navigationItem.hidesSearchBarWhenScrolling = false
    navigationItem.searchController = searchController
    definesPresentationContext = true
  }
}
