import UIKit
import IGListKit

protocol MovieSearchProvider {
  func movieSearch() -> MovieSearchViewController
}

class MovieSearchViewController: UIViewController {
  
  var presenter: MovieSearchPresenter?
  var searchController: UISearchController?
  
  @IBOutlet weak var collectionView: UICollectionView!

  private let listAdapter: ListAdapter
  private let listDataSource: MovieListDataSource
  
  init(listAdapter: ListAdapter,
       listDataSource: MovieListDataSource)
  {
    self.listAdapter = listAdapter
    self.listDataSource = listDataSource
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    title = NSLocalizedString("movie_search.title", comment: "")
    configureCollection()
    configureSearchController()
  }
}

// MARK: - Configuration

extension MovieSearchViewController {
  private func configureCollection() {
    listAdapter.collectionView = collectionView
    listAdapter.dataSource = listDataSource
  }
}

// MARK: - MovieSearchView

extension MovieSearchViewController: MovieSearchView {
  func setNoResults() {
    showNoResultsAlert()
  }
  
  func set(_ error: String) {
    showAlert(for: error)
  }
  
  func set(_ movieResults: [MovieViewModel]) {
    listDataSource.movieResults = movieResults
    listAdapter.performUpdates(animated: true) { _ in
      self.finishInfiniteLoadingAnimation(for: self.collectionView)
    }
  }
  
  func setNoMoreResults() {
    endInfiniteLoading(for: collectionView)
  }
}

// MARK: - SuggestionsViewControllerDelegate

extension MovieSearchViewController: SuggestionsViewControllerDelegate {
  func didTap(_ suggestion: SuggestionViewModel) {
    presenter?.onSearch(query: suggestion.value)
    searchController?.isActive = false
  }
}
