import IGListKit

final class MovieListDataSource: NSObject, ListAdapterDataSource {
  
  var movieResults = [MovieViewModel]()
 
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    return movieResults
  }
  
  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    guard let object = object as? MovieViewModel else { fatalError() }
    return MovieSectionController(movie: object)
  }
  
  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}
