import IGListKit

final class MovieSectionController: ListSectionController {
  
  private var movie: MovieViewModel
  
  init(movie: MovieViewModel) {
    self.movie = movie
  }
  
  override func sizeForItem(at index: Int) -> CGSize {
    return CGSize(width: collectionContext!.containerSize.width, height: MovieResultCell.height)
  }
  
  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let cell = collectionContext?.dequeueReusableCell(withNibName: String(describing: MovieResultCell.self),
                                                            bundle: nil, for: self, at: index) as? MovieResultCell else { fatalError() }
    cell.configure(with: movie)
    return cell
  }
  
  override func didUpdate(to object: Any) {
    movie = object as! MovieViewModel
  }
}
