import UIKit
import Kingfisher

class MovieResultCell: UICollectionViewCell {

  static let height = CGFloat(140)
  
  @IBOutlet weak private var posterImageView: UIImageView!
  @IBOutlet weak private var titleLabel: UILabel!
  @IBOutlet weak private var releaseDateLabel: UILabel!
  @IBOutlet weak private var summaryLabel: UILabel!
  
  func configure(with movie: MovieViewModel) {
    titleLabel.text = movie.name
    releaseDateLabel.text = String(describing: movie.releaseDate)
    summaryLabel.text = movie.overview
    
    posterImageView.kf.setImage(with: movie.posterUrl)
  }
}
