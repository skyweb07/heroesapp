import UIKit

extension MovieSearchViewController {
  func showNoResultsAlert() {
    showAlertWithTitle(
      title: NSLocalizedString("movie_search.error.no_results.title", comment: ""),
      message: NSLocalizedString("movie_search.error.no_results.message", comment: "")
    )
  }
  
  func showAlert(`for` error: String) {
    showAlertWithTitle(
      title: NSLocalizedString("movie_search.error.generic.title", comment: ""),
      message: error
    )
  }
  
  private func showAlertWithTitle(title: String, message: String) {
    let alertController = UIAlertController(
      title: title,
      message: message,
      preferredStyle: .alert
    )
    
    let okAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.default)
    alertController.addAction(okAction)
    
    present(alertController, animated: true)
  }
}
