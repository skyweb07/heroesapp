import UIKit
import UIScrollView_InfiniteScroll

private enum Constraints {
  static let numberOfItemsPerSection = 1
  static let startLoadingAt = 5
}

extension MovieSearchViewController {
  func configureInfiniteLoading(`for` collectionView: UICollectionView) {
    collectionView.addInfiniteScroll { [weak self] collectionView in
      self?.presenter?.onLoadNextPage()
    }
  }
  
  func finishInfiniteLoadingAnimation(`for` collectionView: UICollectionView) {
    collectionView.finishInfiniteScroll()
  }
  
  func endInfiniteLoading(`for` collectionView: UICollectionView) {
    collectionView.removeInfiniteScroll()
  }
}
