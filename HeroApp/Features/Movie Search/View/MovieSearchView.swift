import Foundation

protocol MovieSearchView: class {
  func set(_ movieResults: [MovieViewModel])
  func setNoMoreResults()
  func set(_ error: String)
  func setNoResults()
}
