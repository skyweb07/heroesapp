import UIKit

extension MovieSearchViewController: UISearchBarDelegate {
  func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
    guard let query = searchBar.text, !query.isEmpty else { return }
    presenter?.onSearch(query: query)
    configureInfiniteLoading(for: collectionView)
    
    navigationItem.searchController?.isActive = false
  }
}
