import Foundation

protocol SuggestionsView: class {
  func set(_ suggestions: [SuggestionViewModel])
}
