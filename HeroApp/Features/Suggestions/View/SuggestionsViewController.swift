import UIKit
import IGListKit

protocol SuggestionsProvider {
  func suggestions() -> SuggestionsViewController
}

protocol SuggestionsViewControllerDelegate: class {
  func didTap(_ suggestion: SuggestionViewModel)
}

class SuggestionsViewController: UIViewController {
  
  var presenter: SuggestionsPresenter?
  weak var delegate: SuggestionsViewControllerDelegate? {
    didSet {
      listDataSource.delegate = delegate
    }
  }
  
  @IBOutlet weak private var collectionView: UICollectionView!
  
  private let listAdapter: ListAdapter
  private let listDataSource: SuggestionListDataSource
  
  init(listAdapter: ListAdapter,
       listDataSource: SuggestionListDataSource)
  {
    self.listAdapter = listAdapter
    self.listDataSource = listDataSource
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder aDecoder: NSCoder) { fatalError() }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    presenter?.didLoad()
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    configureCollection()
  }
}

// MARK: - Configuration

extension SuggestionsViewController {
  private func configureCollection() {
    listAdapter.collectionView = collectionView
    listAdapter.dataSource = listDataSource
  }
}

// MARK: - SuggestionsView

extension SuggestionsViewController: SuggestionsView {
  func set(_ suggestions: [SuggestionViewModel]) {
    listDataSource.suggestions = suggestions
    listAdapter.performUpdates(animated: true)
  }
}
