import UIKit

final class MovieSuggestionCell: UICollectionViewCell {

  static var height = CGFloat(50)
  
  @IBOutlet weak private var suggestionLabel: UILabel!

  func configure(with suggestion: SuggestionViewModel) {
    suggestionLabel.text = suggestion.value
  }
}
