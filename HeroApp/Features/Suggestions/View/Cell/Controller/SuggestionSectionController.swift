import IGListKit

final class SuggestionSectionController: ListSectionController {
  
  private var suggestion: SuggestionViewModel
  weak var delegate: SuggestionsViewControllerDelegate?
  
  init(suggestion: SuggestionViewModel) {
    self.suggestion = suggestion
  }
  
  override func sizeForItem(at index: Int) -> CGSize {
    return CGSize(width: collectionContext!.containerSize.width, height: MovieSuggestionCell.height)
  }
  
  override func cellForItem(at index: Int) -> UICollectionViewCell {
    guard let cell = collectionContext?.dequeueReusableCell(withNibName: String(describing: MovieSuggestionCell.self),
                                                            bundle: nil, for: self, at: index) as? MovieSuggestionCell else { fatalError() }
    cell.configure(with: suggestion)
    return cell
  }
  
  override func didUpdate(to object: Any) {
    suggestion = object as! SuggestionViewModel
  }
  
  override func didSelectItem(at index: Int) {
    delegate?.didTap(suggestion)
  }
}
