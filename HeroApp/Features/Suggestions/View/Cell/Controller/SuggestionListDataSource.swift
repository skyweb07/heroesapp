import IGListKit

final class SuggestionListDataSource: NSObject, ListAdapterDataSource {
  
  var suggestions = [SuggestionViewModel]()
  weak var delegate: SuggestionsViewControllerDelegate?
    
  func objects(for listAdapter: ListAdapter) -> [ListDiffable] {
    return suggestions
  }
  
  func listAdapter(_ listAdapter: ListAdapter, sectionControllerFor object: Any) -> ListSectionController {
    guard let object = object as? SuggestionViewModel else { fatalError() }
    let section = SuggestionSectionController(suggestion: object)
    section.delegate = delegate
    return section
  }
  
  func emptyView(for listAdapter: ListAdapter) -> UIView? {
    return nil
  }
}
