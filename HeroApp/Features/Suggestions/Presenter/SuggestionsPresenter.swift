import RxSwift

struct SuggestionsPresenter {
  
  weak var view: SuggestionsView?
  
  private let getSuggestions: GetSuggestionsUseCase
  private let suggestionViewMapper: SuggestionViewMapper
  private let bag = DisposeBag()
  
  init(getSuggestions: GetSuggestionsUseCase,
       suggestionViewMapper: SuggestionViewMapper)
  {
    self.getSuggestions = getSuggestions
    self.suggestionViewMapper = suggestionViewMapper
  }
  
  func didLoad() {
    getSuggestions.execute()
      .map(suggestionViewMapper.map)
      .map(handle)
      .subscribe()
      .disposed(by: bag)
  }
  
  private func handle(_ suggestions: [SuggestionViewModel]) {
    view?.set(suggestions)
  }
}
