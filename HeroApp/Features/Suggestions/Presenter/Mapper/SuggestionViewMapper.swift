import Foundation

struct SuggestionViewMapper: Mappable {
  func map(_ from: Suggestion) throws -> SuggestionViewModel {
    return SuggestionViewModel(
      value: from.value
    )
  }
}
