import UIKit
import IGListKit

// MARK: - SuggestionsProvider

extension Assembly: SuggestionsProvider {
  func suggestions() -> SuggestionsViewController {
    let viewController = SuggestionsViewController(
      listAdapter: listAdapter,
      listDataSource: listDataSource
    )
    viewController.presenter = presenter(for: viewController)
    return viewController
  }
  
  // MARK: - IGListKit
  
  private var listAdapter: ListAdapter {
    return ListAdapter(updater: ListAdapterUpdater(), viewController: nil)
  }
  
  private var listDataSource: SuggestionListDataSource {
    return SuggestionListDataSource()
  }
  
  // MARK: - Presenter
  
  private func presenter(`for` viewController: SuggestionsViewController) -> SuggestionsPresenter {
    var presenter = SuggestionsPresenter(
      getSuggestions: getSuggestions,
      suggestionViewMapper: suggestionViewMapper
    )
    
    presenter.view = viewController
    return presenter
  }
  
  // MARK: - View Mapper
  
  private var suggestionViewMapper: SuggestionViewMapper {
    return SuggestionViewMapper()
  }
}
