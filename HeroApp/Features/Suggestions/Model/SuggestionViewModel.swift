import IGListKit

final class SuggestionViewModel {
  let value: String
  
  init(value: String) {
    self.value = value
  }
}

extension SuggestionViewModel: ListDiffable {
  func diffIdentifier() -> NSObjectProtocol {
    return value as NSObjectProtocol
  }
  
  func isEqual(toDiffableObject object: ListDiffable?) -> Bool {
    guard let object = object as? SuggestionViewModel else { return false }
    return object.value == value
  }
}
