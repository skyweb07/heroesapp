import Foundation

extension Assembly {
  
  var applicationRootController: ApplicationRootController {
    return ApplicationRootController(movieSearchProvider: self)
  }
}
