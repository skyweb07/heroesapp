import UIKit

struct ApplicationRootController {
  
  private let movieSearchProvider: MovieSearchProvider
  
  init(movieSearchProvider: MovieSearchProvider) {
    self.movieSearchProvider = movieSearchProvider
  }
  
  func start(with window: UIWindow?) {
    let navigationController = UINavigationController(
      rootViewController: movieSearchProvider.movieSearch()
    )
    
    window?.rootViewController = navigationController
    window?.makeKeyAndVisible()
  }
}
