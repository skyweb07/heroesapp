import XCTest
@testable import HeroApp

final class PageControllerSpec: XCTestCase {
  
  func test_should_return_first_page_if_no_page_has_been_loaded() {
    let pageController = PageController()
    
    XCTAssertEqual(.first, pageController.nextPage)
  }
  
  func test_should_return_next_page_if_there_are_more_than_one_page() {
    let pageController = PageController()
    pageController.append(testPage, page: .first)
    
    XCTAssertEqual(2, pageController.nextPage.number)
  }
  
  func test_should_return_false_for_loading_next_page_if_there_are_no_pages_loaded() {
    let pageController = PageController()
    
    XCTAssertFalse(pageController.shouldLoadNextPage())
  }
  
  func test_should_return_true_for_loading_next_page_if_there_are_multiple_pages_availables() {
    let pageController = PageController()
    pageController.append(testPage, page: .first)
    
    XCTAssertTrue(pageController.shouldLoadNextPage())
  }
  
  func test_should_restore_to_initial_page_if_page_controller_is_restored() {
    let pageController = PageController()
    pageController.append(testPage, page: .first)
    pageController.restore()
    
    XCTAssertEqual(.first, pageController.nextPage)
  }
  
  func test_should_return_a_list_of_merged_pages_if_page_is_appended() {
    let pageController = PageController()
    pageController.append(testPage, page: .first)
    let mergedPages = pageController.append(testPage, page: Page(number: 2))
    
    let numberOfMergedMovies = 4
    XCTAssertEqual(numberOfMergedMovies, mergedPages.items.count)
  }
}

extension PageControllerSpec {
  private var testPage: PaginatedList<Movie> {
    return PageFactory.page
  }
}
