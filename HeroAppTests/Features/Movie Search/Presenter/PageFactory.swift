import Foundation
@testable import HeroApp

struct PageFactory {
  static var page: PaginatedList<Movie> {
    return PaginatedList(
      currentPage: 1,
      hasNextPage: true,
      totalPages: 10,
      items: movies
    )
  }
  
  static var emptyPage: PaginatedList<Movie> {
    return PaginatedList(
      currentPage: 1,
      hasNextPage: false,
      totalPages: 1,
      items: []
    )
  }
  
  private static var movies: [Movie] {
    return [
      Movie(name: "Batman", overview: "Cool movie", posterUrl: nil, releaseDate: nil),
      Movie(name: "Iron Man", overview: "Cool movie 2", posterUrl: nil, releaseDate: nil),
    ]
  }
}
