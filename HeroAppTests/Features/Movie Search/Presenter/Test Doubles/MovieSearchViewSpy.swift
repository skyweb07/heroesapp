import Foundation
@testable import HeroApp

final class MovieSearchViewSpy: MovieSearchView {
  
  var setNoMoreResultsWasCalled = false
  var setMovieResultsWasCalled = false
  var restoreMovieResultsWasCalled = false
  var setErrorWasCalled = false
  var setNoResultsWasCalled = false
  
  func set(_ movieResults: [MovieViewModel]) {
    setMovieResultsWasCalled = true
    
    if movieResults.isEmpty {
      restoreMovieResultsWasCalled = true
    }
  }
  
  func setNoMoreResults() {
    setNoMoreResultsWasCalled = true
  }
  
  func set(_ error: String) {
    setErrorWasCalled = true
  }
  
  func setNoResults() {
    setNoResultsWasCalled = true
  }
}
