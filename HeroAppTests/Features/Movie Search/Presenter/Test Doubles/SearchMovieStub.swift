import RxSwift
@testable import HeroApp

enum FakeError: Error {
  case fake
}

final class SearchMovieStub: SearchMovieUseCase {
  
  var shouldThrowError = false
  var paginatedList: PaginatedList<Movie>?
  
  func searchMovie(with request: MovieSearchRequest) -> Single<PaginatedList<Movie>> {
    return Single.create(subscribe: { event -> Disposable in
      
      if self.shouldThrowError {
        event(.error(FakeError.fake))
      }
      
      guard let paginatedList = self.paginatedList else { return Disposables.create() }
      event(.success(paginatedList))
      
      return Disposables.create()
    })
  }
}
