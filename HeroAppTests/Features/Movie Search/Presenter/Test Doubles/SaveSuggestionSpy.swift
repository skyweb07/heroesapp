import RxSwift
@testable import HeroApp

final class SaveSuggestionSpy: SaveSuggestionUseCase {
  
  var saveSuggestionWasCalled = false
  
  func execute(with query: String) -> Completable {
    saveSuggestionWasCalled = true
    return .never()
  }
}
