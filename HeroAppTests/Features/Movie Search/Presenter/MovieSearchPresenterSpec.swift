import XCTest
@testable import HeroApp

class MovieSearchPresenterSpec: XCTestCase {
  
  private var sut: MovieSearchPresenter!
  private let pageController = PageController()
  private let movieViewMapper = MovieViewMapper(dateFormatter: DateFormatter())
  private var searchMovie: SearchMovieStub!
  private var saveSuggestion: SaveSuggestionSpy!
  private var movieSearchView: MovieSearchViewSpy!
  
  override func setUp() {
    super.setUp()
    searchMovie = SearchMovieStub()
    saveSuggestion = SaveSuggestionSpy()
    movieSearchView = MovieSearchViewSpy()
    
    sut = MovieSearchPresenter(
      searchMovie: searchMovie,
      saveSuggestion: saveSuggestion,
      movieViewMapper: movieViewMapper,
      pageController: pageController
    )
    sut.view = movieSearchView
  }
  
  override func tearDown() {
    searchMovie = nil
    saveSuggestion = nil
    sut = nil
    movieSearchView = nil
    super.tearDown()
  }
  
  func test_should_restore_search_on_any_search_request() {
    sut.onSearch(query: "Batman")
    
    XCTAssertTrue(movieSearchView.restoreMovieResultsWasCalled)
  }
  
  func test_should_set_error_if_request_failed() {
    searchMovie.shouldThrowError = true
    
    sut.onSearch(query: "Iron Man")
    
    XCTAssertTrue(movieSearchView.setErrorWasCalled)
  }
 
  func test_should_set_movies_if_request_was_ok() {
    searchMovie.paginatedList = PageFactory.page
    
    sut.onSearch(query: "Doctor Strange")
    
    XCTAssertTrue(movieSearchView.setMovieResultsWasCalled)
  }
  
  func test_should_no_load_next_page_if_there_are_no_pages() {
    sut.onLoadNextPage()
    
    XCTAssertTrue(movieSearchView.setNoMoreResultsWasCalled)
  }
  
  func test_should_load_next_page_if_there_are_more_pages() {
    searchMovie.paginatedList = PageFactory.page
    
    sut.onSearch(query: "Doctor Strange")
    sut.onLoadNextPage()
    
    XCTAssertTrue(movieSearchView.setMovieResultsWasCalled)
  }
  
  func test_should_set_no_results_if_no_movies_were_found() {
    searchMovie.paginatedList = PageFactory.emptyPage
    
    sut.onSearch(query: "Unknown movie")
    
    XCTAssertTrue(movieSearchView.setNoResultsWasCalled)
  }
  
  func test_should_save_suggestion_if_its_first_search() {
    searchMovie.paginatedList = PageFactory.page
    
    sut.onSearch(query: "Batman")
    
    XCTAssertTrue(saveSuggestion.saveSuggestionWasCalled)
  }
}
