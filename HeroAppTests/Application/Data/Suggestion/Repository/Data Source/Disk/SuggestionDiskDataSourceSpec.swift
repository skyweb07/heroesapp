import XCTest
import RxBlocking
import RealmSwift
@testable import HeroApp

final class SuggestionDiskDataSourceSpec: XCTestCase {
  
  private var sut: SuggestionDiskDataSource!
  private var realm = try! Realm(configuration: Realm.Configuration(inMemoryIdentifier: "in-memory"))
  
  override func setUp() {
    super.setUp()
    sut = SuggestionDiskDataSource(realm: realm)
  }
  
  override func tearDown() {
    sut = nil
    super.tearDown()
  }
  
  func test_should_save_query_on_disk() throws {
    let _ = sut.save("Batman").toBlocking().materialize()
    
    let suggestions = try sut.getAll().toBlocking().single()
    
    XCTAssertEqual(1, suggestions.count)
  }
}
