import XCTest
import OHHTTPStubs
import RxBlocking
@testable import HeroApp

final class MovieApiDataSourceSpec: XCTestCase {
  private var sut: MovieDataSource!
  
  override func setUp() {
    super.setUp()
    sut = resolver.movieApiDataSource
  }
  
  override func tearDown() {
    sut = nil
    OHHTTPStubs.removeAllStubs()
    super.tearDown()
  }
  
  func test_should_search_for_movies() throws {
    givenMovieSearchResponseIsOk()
    
    let movieList = try sut.searchMovie(with: MovieSearchRequest(query: "batman", page: .first)).toBlocking().single()
    
    let numberOfMoviesInList = 20
    XCTAssertEqual(numberOfMoviesInList, movieList.items.count)
  }
}

// MARK: - Network Stubs

private extension MovieApiDataSourceSpec {
  private func givenMovieSearchResponseIsOk() {
    stub(condition: pathMatches("/3/search/movie/.*")) { _ in
      return OHHTTPStubsResponse(
        jsonObject: Fixture.load("movie.search.response.ok"), statusCode: 200, headers: nil
      )
    }
  }
}
