HeroApp
------------------------

This README provides basic information for some of the most commong tasks you'll have to perform during the development of the app.

## Requirements

* iOS 9.0+
* Xcode 9.4+

## Getting started

Now that you've downloaded the project you'll need to run the app, we can run the app using [Xcode 9](https://developer.apple.com/xcode/downloads/). Make sure to open the `HeroApp.xcworkspace` workspace, and not the `HeroApp.xcodeproj` project.
Currently, the project is compatible with `Xcode 9+` as it's `Swift 4`.

## CocoaPods

[CocoaPods](https://cocoapods.org/) is a dependency manager for Objective-C & Swift, which automates and simplifies the process of using 3rd-party libraries in your projects. To learn more about this amazing tool you can check the their [docs](https://guides.cocoapods.org/). 

Before starting the project execute the following command in the project path in order to download all the project dependencies

`sudo pod install`

## Architecture 🎯

![Header](art/architecture.png)

The app is build on top of `my understanding` of [MVP pattern](https://en.wikipedia.org/wiki/Model%E2%80%93view%E2%80%93presenter) and trying to model the app so it can follow [SOLID principles](https://en.wikipedia.org/wiki/SOLID_(object-oriented_design))

## Connect

All this layers are connected using [RxSwift](https://github.com/ReactiveX/RxSwift/) for the asynchronous part

## Dependency injection 💉

Dependency injection has been achieved by using [Service Locator](https://msdn.microsoft.com/es-es/library/ff648968.aspx) pattern with an `Assembly` that's the one in charge of keeping all the references, I took this approach by using swift extensions of this `Assembly` so it's easier to link dependencies.

## Libraries used in this project

* [RxSwift](https://github.com/ReactiveX/RxSwift/)
* [Moya](https://github.com/Moya/Moya)
* [IGListKit](https://instagram.github.io/IGListKit/)
* [Kingfisher](https://github.com/onevcat/Kingfisher)
* [UIScrollView-InfiniteScroll](https://github.com/pronebird/UIScrollView-InfiniteScroll)
* [Realm](https://github.com/realm/realm-cocoa)

## Test libraries

* [OHHTTPStubs](https://github.com/AliSoftware/OHHTTPStubs)
* [RxBlocking](https://github.com/ReactiveX/RxSwift/tree/master/RxBlocking)


## Contact

[![Twitter](https://img.shields.io/badge/twitter-@skyweb07-red.svg?style=flat)](https://twitter.com/skyweb07)
